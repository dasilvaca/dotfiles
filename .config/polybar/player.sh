#!/bin/sh

player_status=$(mpris-ctl status 2> /dev/null)
player_artist=$(mpris-ctl info %artist_name)
player_titles=$(mpris-ctl info %track_name)

if [ "$player_status" != "" ]; then
	if [ "$player_status" = "Playing" ]; then
	    echo "%{F#FFF} $player_titles"
	elif [ "$player_status" = "Paused" ]; then
	    echo "%{F#FFF} $player_titles"
	elif [ "$player_status" = "Stopped" ]; then
	    echo "%{F#FFF} $player_titles"
	else
	    echo ""
    fi
fi
